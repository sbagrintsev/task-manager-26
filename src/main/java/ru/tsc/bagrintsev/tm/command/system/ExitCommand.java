package ru.tsc.bagrintsev.tm.command.system;

import org.jetbrains.annotations.NotNull;

public class ExitCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.exit(0);
    }

    @NotNull
    @Override
    public String getName() {
        return "exit";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Close program.";
    }

}
