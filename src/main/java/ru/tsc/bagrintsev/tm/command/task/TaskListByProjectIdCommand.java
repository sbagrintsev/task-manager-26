package ru.tsc.bagrintsev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.model.Task;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.io.IOException;
import java.util.List;

public class TaskListByProjectIdCommand extends AbstractTaskCommand {
    @Override
    public void execute() throws AbstractException, IOException {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.print("ENTER PROJECT ID: ");
        @Nullable final String projectId = TerminalUtil.nextLine();
        @Nullable final String userId = getUserId();
        @Nullable final List<Task> tasks = getTaskService().findAllByProjectId(userId, projectId);
        int index = 0;
        for (final Task task : tasks) {
            System.out.println(++index + ". " + task);
        }
    }

    @NotNull
    @Override
    public String getName() {
        return "task-list-by-project-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "List tasks by project id.";
    }
}
